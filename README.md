# CERN Spark Service Docker registry

This repository holds the container images required for Spark on kubernetes deployments with SWAN

- swan - this is the image used for the Spark executors by SWAN on K8S
- spark - this image is legacy, was used for the Spark experimental shuffle service on Spark 2.x 
          and for running Spark as a batch job

### Supported releases
- Build the container for CC7 and Alma9
- Push to `gitlab-registry.cern.ch/db/spark-service/docker-registry`

### Manual build examples

```
cd swan_cc7
docker build -t gitlab-registry.cern.ch/db/spark-service/docker-registry/swan:CC7-20240123 .

cd swan_alma9
docker build -t gitlab-registry.cern.ch/db/spark-service/docker-registry/swan:alma9-20240123 .
```
